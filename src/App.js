import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';

import Search from './components/Search';
import WeatherList from './components/WeatherList';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Search />
          <WeatherList />
        </div>
      </Provider>
    );
  }
}

export default App;
