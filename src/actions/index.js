import axios from 'axios';
const BASE_PATH = 'https://api.openweathermap.org/data/2.5/forecast';

const API_KEY = '5a023f7ec835d7de0baa824f83ecd734';

export const fetchWeather = city => async dispatch => {
  const PARAMS = `?q=${city},us&appId=${API_KEY}`;
  const endpoint = `${BASE_PATH}${PARAMS}`;

  try {
    let { data } = await axios.get(endpoint);
    return dispatch({
      type: 'FETCH_WEATHER_SUCCESS',
      payload: data,
    });
  } catch (e) {
    console.log(e);
  }
};
