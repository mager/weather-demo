import React, { Component } from 'react';
import { Sparklines, SparklinesLine } from 'react-sparklines';

class Chart extends Component {
  render() {
    const { data, color, foo } = this.props;

    return (
      <div style={{ height: '250px', width: '250px' }}>
        <Sparklines data={data}>
          <SparklinesLine
            style={{ strokeWidth: 3, stroke: color, fill: 'none' }}
          />
        </Sparklines>
      </div>
    );
  }
}

export default Chart;
