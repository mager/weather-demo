import React, { Component } from 'react';
import ReactMapboxGL from 'react-mapbox-gl';

const Map = ReactMapboxGL({
  accessToken:
    'pk.eyJ1IjoibWFnZXIiLCJhIjoiY2lobWxvZXRpMG90ZXY1a2x4eG4wNGs1NyJ9.vEVHfV_K15rnm4_niRNHYw',
});

class MapComponent extends Component {
  render() {
    return (
      <Map
        style="mapbox://styles/mapbox/streets-v9"
        center={this.props.coords}
        containerStyle={{
          height: '300px',
          width: '300px',
        }}
      />
    );
  }
}

export default MapComponent;
