import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchWeather } from '../actions';

class Search extends Component {
  state = {
    query: '',
  };

  onQueryChange = event => {
    const query = event.target.value;

    this.setState({
      query,
    });
  };

  onFormSubmit = event => {
    event.preventDefault();

    // Fetch the weather
    this.props.fetchWeather(this.state.query);
  };

  render() {
    return (
      <section className="section">
        <form className="container" onSubmit={this.onFormSubmit}>
          <div className="field has-addons">
            <p className="control has-icons-left is-expanded">
              <input
                className="input"
                type="text"
                placeholder="Enter a city"
                value={this.state.query}
                onChange={this.onQueryChange}
              />
              <span className="icon is-left">
                <i className="fa fa-sun-o" />
              </span>
            </p>
            <div className="control">
              <button className="button is-primary">Submit</button>
            </div>
          </div>
        </form>
      </section>
    );
  }
}

export default connect(null, { fetchWeather })(Search);
