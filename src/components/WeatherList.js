import React, { Component } from 'react';
import { connect } from 'react-redux';

import Map from './Map';
import Chart from './Chart';

class WeatherList extends Component {
  renderWeather() {
    return this.props.weather.length
      ? this.props.weather.map(data => {
          const name = data.city.name;

          const temps = data.list.map(weather => weather.main.temp);
          const pressures = data.list.map(weather => weather.main.pressure);
          const humidities = data.list.map(weather => weather.main.humidity);
          const { lat, lon } = data.city.coord;

          return (
            <tr key={name}>
              <td>
                <Map coords={[lon, lat]} />
              </td>
              <td>
                <Chart data={temps} color="orange" />
              </td>
              <td>
                <Chart data={pressures} color="green" />
              </td>
              <td>
                <Chart data={humidities} color="gray" />
              </td>
            </tr>
          );
        })
      : null;
  }

  render() {
    return (
      <section className="section">
        <table className="table is-fullwidth">
          <thead>
            <tr>
              <th>City</th>
              <th>Temperature (K)</th>
              <th>Pressure (hPa)</th>
              <th>Humdity (%)</th>
            </tr>
          </thead>
          <tbody>
            {this.renderWeather()}
          </tbody>
        </table>
      </section>
    );
  }
}

const mapStateToProps = ({ weather }) => {
  return { weather };
};

export default connect(mapStateToProps, null)(WeatherList);
