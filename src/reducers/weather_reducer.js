export default function(state = [], action) {
  switch (action.type) {
    case 'FETCH_WEATHER_SUCCESS':
      // Do stuff with the data
      return [...state, action.payload];
    default:
      return state;
  }
}
